package tests.base;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.rpc.ServiceException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public abstract class TestBase {

	protected WebDriver driver;
	protected String baseUrl;

	private String firefoxPath;
	//Determine if is running headless
	private String isHeadless;
	
	protected String SCREENSHOTS_BASE_FOLDER;

	protected String verificationErrors;

	/**
	 * Loads the properties from the config.properties file
	 */
	private void loadProperties(){
		Properties p=new Properties();

		try{
			p.load(new FileInputStream("config.properties"));
			
			this.SCREENSHOTS_BASE_FOLDER = p.getProperty("screenshots.basefolder");
			
			this.firefoxPath = p.getProperty("webdriver.firefoxpath");
			this.isHeadless = p.getProperty("webdriver.headless");


		}catch(IOException e){

		}

	}

	@Before
	public void setUp() throws Exception {
		
		//Load properties
		this.loadProperties();
        // ???
		this.verificationErrors="";
		
		if (this.isHeadless.equals("YES")){
			//Set up Firefox hedless with xWindow server
			String xport=System.getProperty("lmportal.xvfb.id", ":99");
			final File firefoxPath = new File(System.getProperty(
					"lmportal.deploy.firefox.path",this.firefoxPath));
			FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);
			firefoxBinary.setEnvironmentProperty("DISPLAY", xport);

			driver = new FirefoxDriver(firefoxBinary,null);
	
		} else { // Run browser in your local environment
			
		}

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@After
	public void tearDown() throws Exception {	
		//Quit webdriver, closes the browser only if running headless
		if(this.isHeadless.equals("YES")){
			driver.quit();
		}
		//Set fail if there are errors
		if(this.verificationErrors != null && !this.verificationErrors.equals("")) {
			fail(this.verificationErrors);
		}
	}
	/**
	 * Takes a screenshot of the page and stores it in the specified folder
	 * @param folder 
	 * Should be the package name
	 * @param fileName
	 * Should be the class name
	 * @throws Exception
	 */
	public void takeScreenshot(String folder,String fileName) throws Exception {
		//Screenshot will be taken only if running headless
		if (this.isHeadless.equals("YES")) {
			File baseDir=new File(this.SCREENSHOTS_BASE_FOLDER+folder);
			//Create a directory with package name if doesn't exists
			if(!baseDir.exists()) baseDir.mkdir();

			//Take a screenshot of the result
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(baseDir.getPath()+ '/' + fileName +".png"));
		}
	}

	/**
	 * Creates or modifies data to meet the test case precondition
	 */
	public abstract void prepareData();
	/**
	 * Undo the changes or data created for the precondition
	 */
	public abstract void undoChanges();

}
