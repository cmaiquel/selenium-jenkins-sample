package tests.test_sample;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

import org.junit.Test;
import org.openqa.selenium.By;

import tests.base.TestBase;

public class Test_01 extends TestBase {

	@Test
	public void testSteps(){
		
		try {

			// Step 1 - Go to http ...
			driver.get("http://google.com/");
			this.takeScreenshot(this.getClass().getPackage().getName(),this.getClass().getSimpleName()+"_step_1");
			Assert.assertTrue("Got http ... opened successfully", true);
		
		}
		catch (AssertionFailedError e){
			//Set the error messages
			this.verificationErrors = e.getMessage();
		}
		catch(Exception e){
			//Take a screenshot of the exception screen
			try {
				this.takeScreenshot(this.getClass().getPackage().getName(), this.getClass().getSimpleName()+"_fail_exception");
			} catch (Exception e1) {
			    e1.printStackTrace();
			}
			//Set the error messages
			this.verificationErrors = e.getMessage();
		}

	}

	@Override
	public void prepareData() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void undoChanges() {
		// TODO Auto-generated method stub
		
	}

}
